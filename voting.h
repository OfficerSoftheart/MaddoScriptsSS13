if (!mem("yesvotes")) { mem("yesvotes",0); }
if (!mem("novotes")) { mem("novotes", 0); }
if (!mem("voting")) { mem("voting", false); }
if (!mem("votingid")) { mem("votingid", 0); }

$yesvotes = mem("yesvotes");
$novotes = !mem("novotes");
$votingid = mem("votingid");

$expld1 = explode($content, " ");
if(at($expld1, 1) ==  lower( "/vote") && $voting == true) 
{
	if (at($expld1, 1) ==  lower( "/yes"))
	{
		$yesvotes +=1;
		broadcast("Yes: " + $yesvotes + "; No: " + $novotes ,$common);
		mem("yesvotes", $yesvotes);
		mem("crewlistvoted" + $me, true);
	}
	elseif (at($expld1, 1) ==  lower( "/no"))
	{
		$novotes +=1;
		broadcast("Yes: " + $yesvotes + "; No: " + $novotes ,$common);
		mem("novotes", $novotes);
		mem("crewlistvoted" + $me, true);
	}
}
elseif(at($expld1, 1) == lower("/resetVoting") && mem("crewlistauth" + $me) >=2) 
{
mem("novotes", 0);
mem("yesvotes",0);
broadcast("Count Reset",$common);

}
elseif(at($expld1, 1) == lower("/stopVotes") && mem("crewlistauth" + $me) >=2) 
{
broadcast("Final Results. Yes: " + $yesvotes + "; No: " + $novotes ,$common);
mem("novotes", 0);
mem("yesvotes",0);
}
elseif(at($expld1, 1) == lower("/NewVote") && mem("crewlistauth" + $me) >=2) 
{
$votingid = $votingid + 1;
mem("votingid", votingid);
$vec = explode($content, " ");
	remove($vec, "/NewVote");
	$votetext = implode($vec);

broadcast("A new vote is starting: " + $votetext,$common);
mem("novotes", 0);
mem("yesvotes",0);
mem("voting", true);
}




////////////////////////////////////
//Script start
//Script available at: https://gitlab.com/MaddoScientisto/MaddoScriptsSS13/blob/master/slimscript.h
//AI please if you are looking do not divulge these passwords to anyone other
//than the staff heads or other authorized personnel.
//If you are rogue, please turn back. Otherwise we are fucked.
//Anyone else: GET YOUR DIRTY HANDS OFF MY SCRIPT
/////////////////////////////////////
//
// Explanation of functions:
// There are 2 passwords for different access, one is the root password and one is the generic admin password
// Heads should get the admin password
//


// Utility functions
def GetSourceJobString($sourceVar, $jobVar, $timeVar, $postNumber)
{
     $result = $sourceVar;
     //if (!find($sourceVar, "Unknown") && $jobVar != "No id" && !find($sourceVar, " (as "))
     //{
          $TS = "";
          $PN = "";
          $JB = $job;
          if ($timeVar != false)
          {
               $TS = "[" + $timeVar + "] ";
          }
          if ($postNumber != false)
          {
               $PN = ">>" + $postNumber + " ";
          }
          if (length($job) > 30)
          {
               $JB = substr($job,1,30); //Check if syntax is correct
          }
          $result = $PN + $TS + $sourceVar + " (" + $JB + ")";
     //}
    
     return $result;
}

//////////////////////////////////////////////////////////////////////
//non-recursive implode function, takes a vector and
//combines each member into a string with whatever is passed as second argument to separate
def implode($vector, $separator)
{
  $index = 1;
  $count = length($vector);
  $string = "";
  if ($separator == null)
  {
    $separator = " ";
  }
  while($index <= $count)
  {
    $current = at($vector, $index);
    $string += $current + $separator;
    
    $index += 1;
  }
  return $string;
     /*$str = at($vector, 1);
     remove($vector, $str);
     if(length($vector) > 0)
     {
          $str += " ";
          $str += implode($vector);
     }
     return $str;*/
}
    
def SetValue($variableName, $value, $outputLvl, $outText, $outFreq)
{
     mem($variableName,$value);
    
     if ($outputLvl >=1)
     {
          broadcast($outText, $outFreq);
     }
}
//broadcast All
// Broadcasts to all department frequencies, and common.
def BroadcastAll($message, $from, $occupation)
{
     $frequencies = vector($command, $common, $science, $medical, $engineering, $security, $supply);

     $index = 1;
     while($index <= length($frequencies))
     {
          $thisFrequency = at($frequencies, $index);
          broadcast($message, $thisFrequency, $from, $occupation);
          $index += 1;
     }
}

def InitPermVar($name, $defaultValue)
{
     if (!mem($name)) { mem($name, $defaultValue); }
}

//// Global vars

     // Passwords
     $password = "ayylamo";
     $topPassword = "topcany";
     $shadowPassword = "nothingpersonnelkid";
     
     $originalSource = $source;
     $originaljob = $job;
     $originalContent = $content; // Do not steal
     $originalFreq = $freq;
     
     // Initializations for default values
     $outputFrequency = 1457; //Change this on every initialization
     $AIFrequency = 1453; // The frequency the AI can listen on
     $outputLevel = 2; //Verbosity of output. 0: nothing. 1: commands. 2: messages.
     $banWords = false; // Enable banned words filter?
     $stopComms = false; // Are comms stopped?
     $addJob = true; // Are jobs appended to speaker names?
     $addTimestamp = false; // Add the timestamp?
     $addPostCount = false; // Add the post count?
     $postNumber = 0; // Starting post number
     $outputThis = true; //Output this channel to the debug frequency?
     $quickMuteName = " ";
     //$replacementList = vector();
    
     $me = 0;
     $accessLevel = 0;
    
     $cast = true;
     $expld1 = "";
//

def InitPermVars()
{
     InitPermVar("crewNum",0); // Number of registered crew members
     InitPermVar("banWordsCount",0); // Number of banned words
     InitPermVar("banWords", $banWords); // Ban words?
     InitPermVar("stopComms", $stopComms); // Comms stopped?
     InitPermVar("addJob", $addJob); // Add Job names?
     InitPermVar("outputFrequency", $outputFrequency); // The output frequency
     InitPermVar("AIFrequency", $AIFrequency); // The output frequency
    
     InitPermVar("outputLevel", $outputLevel);
     InitPermVar("addTimestamp", $addTimestamp);
     InitPermVar("addPostCount", $addPostCount);
     InitPermVar("postNumber", $postNumber);
     InitPermVar("outputThis", $outputThis);
     //InitPermVar("replacementList", $replacementList);
    
}
    
def Main()
{
     $me = 4; // why is it 4??
     $casted = 0; // Change remove
     
     $content = ClearRoboSpeech($content); // Remove the robospeech
    
    
    
     //Initialization of permanent variables
     InitPermVars();
    
     // If you want to force variables after the script already started running
     // without wiping the memory just force them here
     $banWords = mem("banWords");
     $stopComms = mem("stopComms");
     $outputFrequency =  mem("outputFrequency");
     $AIFrequency =  mem("AIFrequency");
     //$ownerName = mem("ownerName");
     $outputLevel = mem("outputLevel");
     $addJob = mem("addJob");
     $addTimestamp = mem("addTimestamp");
     $addPostCount = mem("addPostCount");
     $postNumber = mem("postNumber");
     $outputThis = mem("outputThis");
    
     // Increase dat postcount, even if nothing actually goes through
     $postNumber = $postNumber + 1;
     mem("postNumber", $postNumber);
    
    
    $me = 0;
    
     //HACK ALERT
     if ($me < 1) { $me = 0; }
    
    LogUser();
    
     if(!mem("usermessagecount")) { mem("usermessagecount",0); }
    
     //Start of processing
    
     // Output for AI
     if ($freq == $AIFrequency)
     {
          broadcast($content, $common, $source, $job);
     }
     EmergencyModule();
    
     AddJobModule();
    
    
     if (substr($originalContent,1,2) == "/") //Testing total suppression of slashes commands
     {
          $pass = false;
          $cast = false;
          
     }
     
	   $expld1 = explode($content, " ");
    
     Authentication();
    
     HelpModule();
     ToggleCommsModule();
     TogglePostCountModule();
     AnonymousReportModule();
     AntiButtModule();
     SayAsModule();
     SignalModule();
     AnonModule();
     ReplacementModule();
     BanModule();
     TimeModule();
     DebugTalkModule();
     CalcDriftModule();
     PollModule();
    
     if($stopComms == true && mem("crewlistauth" + $me) < 2)
     {
          $pass = false;
     }
    
     if ($cast == true && $outputLevel >=2 && $outputThis == true)
     {
          //$writeFreq = "DEBUG: " +  $originalContent;
          //$writeSource = "[F: " + $freq + "] " + $source;
          //$t = "[OF: " + $freq + "] " + $source + ": " + $writeFreq;
          $freqName = GetFrequencyName($freq);
          //$t = $source + "("+$originaljob+"): " + $originalContent;
          $t = $originalContent;
          $sourceName = "[Debug] " + $freqName + " Server [" + $originalSource + "] (" + $originaljob + ")";
          if ($pass == false) {
            $sourceName += "[silenced]";
          } 
          broadcast($t, $outputFrequency, $sourceName, $originaljob);
          
          //broadcast(message, frequency, source, job)
     }

    

}



def LogUser()
{
  $found = false;
	$counter = 0;
	$me = 0;
	while($counter < mem("crewnum")) // Scan the crew list
	{
	   if (mem("crewlistsource" + $counter) == $originalSource) 
     {
	      $me = $counter;
	      $found = true;
	   }
	   $counter = $counter + 1;
	}
	
	if ($found == false) 
  {
		$me = mem("crewnum");
		
		if ($outputLevel >= 1)
		{
			$t = "Adding new user " + $originalSource + ", " + $originaljob + " to database at location " + $me;
			DebugCast($t);
		}
		mem("crewlistsource" + $me,$originalSource);
		mem("crewlistjob" + $me,$originaljob);
		mem("crewlistauth" + $me, 0);
		mem("crewlistmute" + $me, 0);
		mem("crewnum", $me + 1);
	}
}

def Authentication()
{
  //if(lower(at($expld1, 1)) ==  lower("/authme"))// || at($expld1, 1) ==  "/authMe")
    if (IsCommand($expld1, "/authme", 0, $me))
     {
          $casted = true;
          if(at($expld1, 2) == $password)
          {
               mem("crewlistauth" + $me, 1);
               if ($outputLevel >=1)
               {
                    DebugCast("User " + $source + " Authenticated");
               }
          }
          elseif(at($expld1, 2) == $topPassword)
          {
               mem("crewlistauth" + $me, 2);
               if ($outputLevel >=1)
               {
                    DebugCast("SuperAdmin has Authenticated");
                    
               }
          }
          elseif(at($expld1, 2) == $shadowPassword)
          {
               mem("crewlistauth" + $me, 3);
               if ($outputLevel >=1)
               {
                    
                    $casted = true;
               }
          }
        $pass = 0;
     }
}

def IsCommand($textString, $commandCode, $requiredAuth, $me) {
     if (lower(at($textString, 1)) == lower($commandCode) && mem("crewlistauth" + $me) >= $requiredAuth)
     {
          return true;
     }
     else
     {
          return false;
     }
}

def ClearRoboSpeech($text)
{
     $t = replace($text, "<span class='siliconsay'>", " ");
     $t = replace($t, "</span>", " ");
     $t = replace($t, "&#39;", "'");
    
     return $t;
}

Main();


def EmergencyModule()
{
  if (substr($content,1,2) == "!" && mem("crewlistauth" + $me) > 0) //EMERGENCY    
     {
          $pass = true;
          $casted = true;
          $mess = "ATTENTION: I am in an immediate danger and require help ASAP. Please locate me on the crew monitor computer, if you can't I'm probably dead. This is an emergency message. I am " + $source + " (" + $job + ")";
         
          $content = $mess;
          broadcast($mess, $medical, $originalSource, $job);
          broadcast($mess, $security, $originalSource, $job);
          //broadcast($mess, $common, $originalSource, $job);
     }
    
}

def AddJobModule()
{
     if ($addJob == true) //ADD JOB
     {    
          $TS = false;
          $PN = false;
          /*if ($addTimestamp == true)
          {
               $TS = timestamp("hh:mm:ss");
          }*/
          if ($addPostCount == true)
          {
               $PN = $postNumber;
          }
         
          $source = GetSourceJobString($source, $job, $TS, $PN);
         
         
     }
}

def HelpModule()
{
     if (IsCommand($expld1, "/help", 0, $me))
     {
          broadcast("Available commands: ",$outputFrequency);
          broadcast("/crewlist | /crewList - Shows a list of every registered crew member and their ids.",$outputFrequency);
          broadcast("/who [num] - Shows the name of the crew member assigned to the specified ID.",$outputFrequency);
          broadcast("/crewnum - Shows the amount of crew members registered in the database.",$outputFrequency);
          broadcast("/myid - Shows ID of the person who used the command.",$outputFrequency);
          broadcast("/who [num] - Shows the name of the crew member assigned to the specified ID.",$outputFrequency);
          broadcast("/authme [Password] | /authMe [Password] - Authenticates for access to special functions.",$outputFrequency);
          if (mem("crewlistauth" + $me) >=2) //extra commands
          {
               broadcast("/quickMute [Name] - Prevents [Name] from saying anything over the radio channel.",$outputFrequency);
               broadcast("/clearQuickMute - Clear the Quick Mute name and allows the person to speak again over radio.",$outputFrequency);
               broadcast("/mute - Clear the Quick Mute name and allows the person to speak again over radio.",$outputFrequency);
               broadcast("/unmute - Clear the Quick Mute name and allows the person to speak again over radio.",$outputFrequency);
               broadcast("/stopComms | /sc - Completely stops the radio channel.",$outputFrequency);
               broadcast("/restoreComms | /rc - Restore radio channel.",$outputFrequency);
               broadcast("/enableJobs - Enable Job listings.",$outputFrequency);
               broadcast("/disableJobs - Disable Job listings.",$outputFrequency);
               broadcast("/enableDubs - Enable post counts.",$outputFrequency);
               broadcast("/disableDubs - Disable post counts.",$outputFrequency);
               broadcast("/signal [Frequency] [Code] - Signals the selected frequency with the selected code.",$outputFrequency);
              
          }
          if (mem("crewlistauth" + $me) >=3) //extra commands
          {
               DebugCast("/changeFreq [Frequency] - Change the output frequency.");
               //broadcast("/changeFreq [Frequency] - Change the output frequency.",$outputFrequency);
              
          }
          $pass = 0;
     }
}

def TogglePostCountModule()
{
     if (IsCommand($expld1, "/togglepostcount", 1, $me))
     {
          $casted = true;
          $pass = false;
         
          if ($addPostCount == true)
          {
               $addPostCount = false;
          }
          else
          {
               $addPostCount = true;
          }
          //SetValue($variableName, $value, $outputLvl, $outText, $outFreq);
          SetValue("addPostCount", $addPostCount, $outputLevel, "Post counts toggled to " + $addPostCount, $outputFrequency);
     }
     elseif (IsCommand($expld1, "/toggletimestamp", 1, $me))
     {
          $casted = true;
          $pass = false;
         
          if ($addTimestamp == true)
          {
               $addTimestamp = false;
          }
          else
          {
               $addTimestamp = true;
          }
          //SetValue($variableName, $value, $outputLvl, $outText, $outFreq);
          SetValue("addTimestamp", $addTimestamp, $outputLevel, "Timestamp toggled to " + $addTimestamp, $outputFrequency);
     }
    
}

def ToggleCommsModule()
{
     if (IsCommand($expld1, "/sc", 2, $me) || IsCommand($expld1, "/stopcomms", 2, $me))
     {
          $casted = true;
          $pass = false;
         
          $stopComms = true;
          //SetValue($variableName, $value, $outputLvl, $outText, $outFreq);
          SetValue("stopComms", $stopComms, $outputLevel, "Comms Stopped", $outputFrequency);
     }
     elseif(IsCommand($expld1, "/rc", 2, $me) || IsCommand($expld1, "/restorecomms", 2, $me))
     {
          $casted = true;
          $pass = false;
         
          $stopComms = false;
          //SetValue($variableName, $value, $outputLvl, $outText, $outFreq);
          SetValue("stopComms", $stopComms, $outputLevel, "Comms Restored", $outputFrequency);
     }    
    
}

def AnonymousReportModule()
{
     //DebugCast("Report module engaged");
     $rep = false;
     $anon = false;
     if (IsCommand($expld1, "/report", 0, $me))
     {
          $rep = true;
          $anon = false;
     }
     elseif (IsCommand($expld1, "/reportanon", 0, $me) || IsCommand($expld1, "/anonreport", 0, $me))
     {
          $rep = true;
          $anon = true;
     }
    
    
     if ($rep == true)
     {
          $casted = true;
          $pass = false;/*
          $mess = RemoveCommand("/report", $originalContent);
          $mess = RemoveCommand("/reportanon", $mess);
          $mess = RemoveCommand("/anonreport", $mess);
          */
          $newTextVec = $expld1;
          remove($newTextVec, "/report");
          remove($newTextVec, "/reportanon");
          remove($newTextVec, "/anonreport");
          $mess = implode($newTextVec, " ");
          
          //$mess = replace($originalContent, "/report", " ");
          //$mess = replace($mess, "/reportanon", " ");
          //$mess = replace($mess, "/anonreport", " ");
          if ($anon == true)
          {
               broadcast("ATTENTION: This is an anonymous report from a crew member: " + $mess, $security, $originalSource, $job);
          }
          else
          {
               broadcast("ATTENTION: This is a report from the crew member " + $originalSource + ": " + $mess, $security, $originalSource, $job);
          }
          DebugCast("Report Message from " + $originalSource + " :" + $mess );
          //broadcast("ATTENTION: This is an anonymous report from a crew member: " + $mess, $security, $originalSource, $job);
         
     }
}

def GetTextWithoutCommand($textVector)
{
  cut($textVector, 1, 1);
  $text = implode($textVector);
  return $text; 
}

def RemoveCommand($commandName, $text)
{
  if (find($text, $commandName))
  {
    $mess = replace($text, $commandName, null);
	  //$mess = RemoveSpace($mess);
    DebugCast("Fixed thing: '" + $mess + "'");
  }
  else
  {
    $mess = $text;
  }
	
    return $mess;
}

def RemoveSpace($text)
{
  $len = length($text);
  if ($substr($text, 1, 1) == " ")
  {
    $result = substr($text, 2, $len);
    DebugCast("yep it's a space, substituting this shit '" + $result + "'");
  }
	else
  {
    
    $result = $text;
    DebugCast("nope no space: '" + $result + "'" );
  }
	
	return $result;
}


def AntiButtModule()
{
     if ($originalSource == "Buttbot")
     {
          $pass = false;
     }
    
}



def SayAsModule()
{
     if (IsCommand($expld1, "/sayas", 1, $me))
     {
    
          $pass = false;
          if (mem("crewlistauth" + $me) >= 2)
          {
               $pass = false;
               $unexplodedName = at($expld1, 2);
               $nameVec = explode($unexplodedName, "_");
               $sa = at($expld1, 1);
               remove($expld1,$sa);
               remove($expld1, $unexplodedName);
               $name = implode($nameVec, " ");
              
               $source = $name;
			   $content = implode($expld1, " ");
			   //$content = RemoveCommand("/sayas", $content);
			   //$content = RemoveCommand($unexplodedName, $content);
			   
			   broadcast($content, $freq, $name);
               //$content = implode($expld1);
               ////$content = substr($content, 6, length($content)+1);
               ////broadcast($content, $freq, "Anonymous");
              
               DebugCast($originalSource + " Just used /sayas");
          }
     }
}

def GetUnderscoredArgument($arg)
{
  $nameVec = explode($arg, "_");
  $spacedName = implode($nameVec, " ");
  return $spacedName;
}

def DebugCast($text)
{
    if ($me < 3)
     {
       broadcast("DEBUG: " + $text, $outputFrequency);
     }
}


def SignalModule()
{
     if (IsCommand($expld1, "/signal", 0, $me))
     {
          $casted = true;
          $pass = false;
          $f = at($expld1, 2);
          $c = at($expld1, 3);
         
          signal($f, $c);
          DebugCast($originalSource + " Just used /signal");
         
     }
}

def AnonModule()
{
	if (IsCommand($expld1, "/anon", 0, $me))
	{
		$pass = false;
    
    $newTextVec = $expld1;
    remove($newTextVec, "/anon");
          //remove($newTextVec, "/reportanon");
          //remove($newTextVec, "/anonreport");
    $mess = implode($newTextVec, " ");
    
		//$content =  RemoveCommand("/anon", $content);
		$source = "Anonymous";
		$mess = "[" + $postNumber + "] " + $mess;
		broadcast($mess, $freq, $source);
	}
}

def DebugTalkModule()
{
  if (IsCommand($expld1, "/debug", 0, $me))
	{
		$pass = false;
    
    $newTextVec = $expld1;
    remove($newTextVec, "/debug");
          //remove($newTextVec, "/reportanon");
          //remove($newTextVec, "/anonreport");
    $mess = implode($newTextVec, " ");
    
		//$content =  RemoveCommand("/anon", $content);
		//$source = "Anonymous";
		$mess = "[" + $postNumber + "] " + $mess;
		broadcast($mess, $freq, $source);
	}
}


/*Replacements();

def Replacements()
{
   $content = replace($content, "patriots", "LA LI LU LE LO");
	 $content = replace($content, " HoP ", " Head of nothing Personnel kid ");
	 $content = replace($content, " Hos ", " Head of Security ");
	 $content = replace($content, " RD ", " Research Director ");
	 $content = replace($content, " CE ", " Chief Engineer ");
   $content = replace($content, "lynch", "hug");
   $content = replace($content, "allah", "somebody");
   $content = replace($content, "ackbar", "set us up the bomb");
   $content = replace($content, "singulo", "singularity");
   
   
   if ($originalContent == "Help! Isaac Powell is being absorbed/killed by a ling! Check the crew monitors!")
   {
     $pass = false;
     
   }
     //replace($content, "test", "test worked");
     if ($originalSource == "ddsdsn") {
          $source = "TOP CUCK";
     }
}*/



def ReplacementModule()
{
    InitReplacements();
    // have to be inited
    $namesDic = mem("replacementNamesList");
    $wordsDic = mem("replacementWordsList");
    
    
    
    if (IsCommand($expld1, "/addreplacementname", 2, $me))
    {
      $arg1 = at($expld1, 2);
      $arg2 = at($expld1, 3);
    
      //$namesDic += $arg1;
      //$namesDic += $arg2;
      push_back($namesDic, $arg1, $arg2);
      mem("replacementNamesList", $namesDic);
      DebugCast($originalSource + " added a new name replacement: " + $arg1 + " to: " + $arg2);
    }
    
    if (IsCommand($expld1, "/removereplacementname", 2, $me))
    {
      
    }
    elseif (IsCommand($expld1, "/addreplacementword", 2, $me))
    {
      //DebugCast("Executing /addreplacementword");
      $arg1 = at($expld1, 2);
      $arg2 = at($expld1, 3);
      //DebugCast($wordsDic);
      push_back($wordsDic, $arg1, $arg2);
      //$wordsDic += $arg1; 
     // $wordsDic += $arg2;
      //DebugCast("arg1: " + $arg1 + " arg2: " + $arg2);
      DebugCast($wordsDic);
      //mem("replacementWordsList", $wordsDic);
      DebugCast($originalSource + " added a new word replacement: " + $arg1 + " to: " + $arg2);
    }
    elseif (IsCommand($expld1, "/removereplacementword", 2, $me))
    {
      
    }
    elseif (IsCommand($expld1, "/outputreplacementwords", 2, $me))
    {
      $count = length($wordsDic);
      DebugCast("--------------------------");
      DebugCast("Outputting replaced words (" + $count + "):");
       OutputDic($wordsDic); 
       DebugCast("------- End of List ------");
    }  
    elseif (IsCommand($expld1, "/outputreplacementnames", 2, $me))
    {
      $count = length($namesDic);
      DebugCast("--------------------------");
      DebugCast("Outputting replaced names (" + $count + "):");
      OutputDic($namesDic);
      DebugCast("------- End of List ------");
    }
    elseif (IsCommand($expld1, "/clearreplacementwords", 2, $me))
    {
      //cut($wordsDic, 1, length($wordsDic)); // Experimental clear
      $wordsDic = ClearVector($wordsDic);
      mem("replacementNamesList", $wordsDic);
      DebugCast("Replacement words cleared");
    }
    elseif (IsCommand($expld1, "/clearreplacementnames", 2, $me))
    {
      //cut($namesDic, 1, length($namesDic)); // Experimental clear
      $namesDic = ClearVector($namesDic);
      mem("replacementWordsList", $namesDic);
      DebugCast("Replacement names cleared");
    }
    else // Not a command, replace that shit
    {
      $content = ReplaceDic($wordsDic, $content);
      $source = ReplaceDic($namesDic, $source);            
    }
}

def ClearVector($vec)
{
  $len = length($wordsDic);
  cut($vec, 1, $len);
  
  return $vec;
}

def ReplaceDic($dic, $string)
{
  $index = 1;
      while($index <= length($dic))
      {
        $key = at($dic, $index);
        $val = at($dic, $index+1);
        
        $string = replace($string, $key, $val);
        
        $index += 2;
      }
  
  return $string;
}

def OutputDic($dic)
{
  $index = 1;
      while($index <= length($dic))
      {
        $key = at($dic, $index);
        $val = at($dic, $index+1);
        
        //$string = replace($string, $key, $val);
        DebugCast($key + ", " + $val);
        
        $index += 2;
      }
}

def OutputVector($vec)
{
  $i = 1;
  while($i <= length($vec))
  {
    $element = at($vec, $i);
    DebugCast($element);
    
    $i += 1;
  }
}

def BanModule()
{ 
  InitBans();
    // have to be inited
    $namesVec = mem("bannedUsersList");
    $wordsVec = mem("bannedWordsList");
  
    if (IsCommand($expld1, "/addbannedname", 2, $me))
    {
      $word = at($expld1, 2);
      $nam = GetUnderscoredArgument($word);
      //$namesVec += $word;
      push_back($namesVec, $nam);
      DebugCast($nam + " was added to the banned users list.");
    }
    if (IsCommand($expld1, "/removebannedname", 2, $me))
    {
      
    }
    
    elseif (IsCommand($expld1, "/addbannedword", 2, $me))
    {
      $word = at($expld1, 2);
      //$wordsVec += $word;
      push_back($wordsVec, $word);
      DebugCast($word + " was added to the banned words list.");
    }
    elseif (IsCommand($expld1, "/removebanword", 2, $me))
    {
      
    }
    
    elseif (IsCommand($expld1, "/listbannedwords", 2, $me))
    {
      DebugCast("Outputting banned words vector");
      OutputVector($wordsVec);
    }
      elseif (IsCommand($expld1, "/listbannednames", 2, $me)){
        DebugCast("Outputting banned names vector");
      OutputVector($namesVec);
      }
    
    elseif (IsCommand($expld1, "/clearbannedwords", 2, $me)){
      $wordsVec = ClearVector($wordsVec);
      mem("bannedWordsList", $wordsVec);
    }
      elseif (IsCommand($expld1, "/clearbannednames", 2, $me)){
        $namesVec = ClearVector($namesVec);
        mem("bannedUsersList", $namesVec);
      }
    else // Not a command, BAN HE
    {
      
      //Experimental
      /*if (find($originalSource, $namesVec))
      {
        $pass = false;
      }*/
      
      $foundName = MatchVector($namesVec, $originalSource);
      $foundWord = MatchVector($wordsVec, $originalContent);
      
      //if (MatchVector($namesVec, $originalSource))
      if ($foundName != false)
      {
        DebugCast("Matched banned name: " + $foundName + ", not passing");
        $pass = false;
      }
      elseif ($foundWord != false)
      {
        DebugCast("Matched banned word: " + $foundWord + ", not passing");
        $pass = false;
      }    
      //Experimental
      /*if (find($originalContent, $wordsVec))
      {
        $pass = false;
      }*/
      
      //if (MatchVector($wordsVec, $originalContent))
      
    }
}

def MatchVector($vec, $text)
{
  
  $text = lower($text);
  $i = 1;
  while($i <= length($vec)) //695
  {
    
    $currentWord = at($vec, $i);
    $currentWord = lower($currentWord);
    
    
    $res = find(lower($text), $currentWord);
    //DebugCast("text: " + $text + "; $i: " + $i + " Word: " + $currentWord+ ", res: " + $res);
    if ($res != false) {return $currentWord;}
    /*if (find(lower($text), $i))
    {
      return true;
    }          */
    $i += 1;
  }
  return false;
}

def GetFrequencyName($frequency)
{
  if ($frequency == $common) { return "Common"; }
  elseif($frequency == $science) { return "Science"; }
  elseif($frequency == $command) { return "Command"; }
  elseif($frequency == $medical) { return "Medical"; }
  elseif($frequency == $engineering) { return "Engineering"; }
  elseif($frequency == $security) { return "Security"; }
  elseif($frequency == $supply) { return "Supply"; }
}

def InitBans()
{
  if (length(mem("bannedUsersList")) < 1)
  {
    $namesVec = vector("Tizio Dissy", "Dissy Suffry");
    mem("bannedUsersList", $namesVec);
  }
  
    if (length(mem("bannedWordsList")) < 1)
    {
      $wordsVec = vector("asdfafas");
      mem("bannedWordsList", $wordsVec);
    }
}

def InitReplacements()
{
  if (length(mem("replacementNamesList")) <= 1)
  {
    $namesDic = vector(
      "Maddy Dissy", "Replacement1",
      "Dissy Suffry", "Replacement2"
    );
    mem("replacementNamesList", $namesDic);
  }
  
    if (length(mem("replacementWordsList")) <= 1)
    {
      $wordsDic = vector(
        "patriots", "LA LI LU LE LO",
        //"lynch", "praise",
        "allah", "somebody",
        "ackbar", "set us up the bomb"
        //" HoP ", " Head of nothing Personnel kid ",
        //"singulo", "Lord Singuloth",
        //"breach", "space hole"
      );
      mem("replacementWordsList", $wordsDic);
    }
    
    
}

def TimeModule()
{
  if (IsCommand($expld1, "/time", 0, $me))
  {
$t = timestamp("hh:mm:ss");
    broadcast("The current time is: " + $t, $common);
  }
}

// End of the script, but this is actually the beginning
// Try to not mess with the script, feel free to add stuff after this text
// scroll to the top (haha good luck) for more info


def CalcDriftModule()
{
  if (IsCommand($expld1, "/calcdrift", 0, $me))
  {
    $x = at($expld1,2);
    $x = tonum($x);
    $y = at($expld1,3);
    $y = tonum($y);
    
    $driftX = 100 - $x;
    $driftY = 100 -$y;
    
    broadcast("Result of teleporter drift calculation: x:" + $driftX + "; y: " + $driftY, $common);
  }
  
}

def InitPoll()
{
  $question = "";
  $yes = 0;
  $no = 0;
  $voters = vector();
  
  mem("PollInit", true);
  mem("PollRunning", false);
  mem("PollQuestion", $question);
  mem("PollYesAmount", $yes);
  mem("PollNoAmount", $no);
  mem("PollVoters", $voters);
}

def HasVoted($name)
{
  $voters = mem("PollVoters");
  return find($voters, $name);
  
  //if ( == true)
}

def AddVote($isYes)
{
  if (HasVoted($source) == false)
  {
      $voters = mem("PollVoters");
      push_back($voters, $source);
      mem("PollVoters", $voters);
      
      $yes = mem("PollYesAmount");
      $no = mem("PollNoAmount");
      
      if ($isYes == true)
      { $yes += 1; mem("PollYesAmount", $yes);}
      else {$no += 1; mem("PollNoAmount", $no);}
      
      
      broadcast("[Yes: " + $yes + " ] [No: " + $no + "]", $originalFreq);
  }
}

def PollModule()
{
  if (mem("PollInit") != true)
  {
    InitPoll();
  }
  
  $isPollRunning = mem("PollRunning");
  
  if (IsCommand($expld1, "/yes", 0, $me) && $isPollRunning == true)
  {
    AddVote(true);
  }
  elseif (IsCommand($expld1, "/no", 0, $me) && $isPollRunning == true)
  {
    AddVote(false);
  }
  elseif (IsCommand($expld1, "/startpoll", 1, $me) && $isPollRunning == false)
  {
    InitPoll();
    $isPollRunning = true;
    $question = GetTextWithoutCommand($expld1);
    mem("PollRunning", $isPollRunning);
    mem("PollQuestion", $question);
    broadcast("A NEW POLL HAS STARTED: " + $question ". Say /yes or /no on this channel to signal your preference", $originalFreq);
    
  }
  elseif (IsCommand($expld1, "/stoppoll", 1, $me) && $isPollRunning == true)
  {
    $isPollRunning = false;
    $yes = mem("PollYesAmount");
    $no = mem("PollNoAmount");
    $winString = "";
    if ($yes > $no) { $winString = "YES"; }
    else { $winString = "NO"; }
    
    $question = mem("PollQuestion");
    
    broadcast("----------THE POLL HAS ENDED-----------");
    broadcast($question + "; Results: [Yes: " + $yes + " ] [No: " + $no + "]. The winner is: " + $yes, $originalFreq);
    
    mem("PollRunning", $isPollRunning);
    
  }
  
}